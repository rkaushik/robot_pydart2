from pydart2.world import *
import numpy as np

class Robot_world(World):
    def __init__(self, step, skel_path=None):
        self.skeletons = list()
        self.control_skel = None
        self.recording = None
        self.robots = list()

        if skel_path is not None:
            skel_path = os.path.realpath(skel_path)
            self.id = papi.createWorldFromSkel(skel_path)
            self.set_time_step(step)
            nskels = self.num_skeletons()
            for i in range(nskels):
                self.add_skeleton_from_id(i)
        else:
            self.id = papi.createWorld(step)

        self.reset()
        self.enable_recording()

    def step(self):
        
        papi.world__step(self.id)
        self._frame += 1
        self.collision_result.update()
        if self.recording:
            self.recording.bake()
