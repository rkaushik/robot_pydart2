from robot_pydart2.robot_world import *
import pydart2.utils.transformations as trans

class Simulator():
    def __init__(self, dt, title=None, default_camera=None, simulation_time = 40):
        #dt is both display framerate as well as dynamics simulation timestep
        #dt is in
        self.dt = dt
        self.sim = Robot_world(dt)
        self.sim.set_collision_detector(self.sim.BULLET_COLLISION_DETECTOR)
        self.skels = list()
        self.is_animating = False
        self.frame_index = 0
        self.capture_index = 0

        self.robots = list()
        self.is_simulating = True
        self.default_camera = default_camera
        self.display_frame_rate = int(dt*1000.) #in ms
        self.simulation_time = simulation_time
        self.time_counter = 0.
        self.reset()
    
    def reset(self):
        self.is_simulating = False
        self.is_animating = False
        self.frame_index = 0
        self.capture_index = 0
        self.time_counter = 0.

        for bot in self.robots:
            bot.reset()

    def num_robots(self):
        return len(self.robots)
    
    def get_robots(self):
        return self.robots

    def step(self):
        if self.time_counter > self.simulation_time:
           self.is_simulating = False
        else:
            for robot in self.robots:
                if robot.controller is not None and self.time_counter >= robot.control_timer:
                    robot.control_timer += robot.control_rate
                    commands = robot.compute_next_commands()
                    robot.set_commands(commands) 
                else:
                    robot.set_commands(robot.commands) #set old command
            self.sim.step()
            
    
    def step(self, actions): 
        #action should me 2 Dimensional np array np.array([[actions_bot1], [actions_bot2],...])
        # 1st dim should be same as num of robots
        # This method doesnt consider control frequency
        # For every call, the actions are set and one world step is done

        assert (actions.ndim == 2), "Commands should be 2 dimensional numpy array"
        i = 0
        for robot in self.robots:
            action = actions[i]
            i = i + 1
            robot.control_timer += robot.control_rate
            commands = action
            robot.set_commands(commands) 
        self.sim.step()
        self.time_counter += self.dt
    
    def idle(self, ):
        if self.sim is None:
            return

        if self.is_simulating:
            self.step()
            self.time_counter += self.dt

        elif self.is_animating:
            self.frame_index = (self.frame_index + 1) % self.sim.num_frames()
            if hasattr(self.sim, "set_frame"):
                self.sim.set_frame(self.frame_index)
        
    
    def run(self, ):
        # Run
        while self.is_simulating: 
            self.idle()  
    
    #Unactuated skeletons to the simulation
    def add_skeleton(self,skel_path, rpy_and_pos, fixed_to_world = True):
        skel = self.sim.add_skeleton(skel_path)
        pose = skel.positions()
        if not fixed_to_world:
            for i in range(6):
                pose[i] = rpy_and_pos[i]
            skel.set_positions(pose)
        
        else:
            Rot = trans.euler_matrix(rpy_and_pos[0], rpy_and_pos[1], rpy_and_pos[2], 'rxyz')
            Tr = trans.translation_matrix(rpy_and_pos[3:])
            T = trans.concatenate_matrices(Tr, Rot)
            skel.set_root_joint_to_weld()
            # print skel.root_bodynode().parent_joint.type();
            skel.root_bodynode().parent_joint.set_transform_from_parent_body_node(T);
        
        for j in skel.joints:
                j.set_position_limit_enforced()

        self.skels.append(skel)
