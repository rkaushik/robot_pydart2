import numpy as np

class Robot(object):

    FORCE, VELOCITY, POSITION, SERVO, FREE = list(range(5))
    #FORCE : command type is force and force is set
    #POSITION : command type is position and position is set
    #VELOCITY : command type is velocity and velocity is set
    #SERVO : command type is velocity and force is set by a PI controller
    #FREE : command is not set

    def __init__(self, skel_path, name,  simulator, controller = None, control_rate= 0.01, enforce_position_limits=True):
        self.skel_path = skel_path
        self.name = name
        self.skel = simulator.sim.add_skeleton(skel_path)
        simulator.robots.append(self)
        if enforce_position_limits:
            for j in self.skel.joints:
                j.set_position_limit_enforced()
        
        self.actuator_type = Robot.FORCE
        self.controller = controller
        self.skel.set_root_joint_to_weld()
        self.integral = 0.
        self.control_rate = control_rate
        self.control_timer = 0. 
        self.init_pos = np.zeros(self.skel.ndofs)

    def reset(self):
        #reset commands
        self.set_commands(np.zeros(self.num_dofs()))
        #reset positions
        self.set_positions(self.init_pos)
        #reset_velocities
        self.set_velocities(np.zeros(self.num_dofs()))
        #reset forces
        self.set_forces(np.zeros(self.num_dofs()))
        #reset SERVO controller
        self.integral = 0.
        #reset controller
        self.control_timer = 0. 
        if self.controller is not None:
            self.controller.reset()

    def set_init_position(self, pos):
        assert (self.num_dofs() == pos.size and pos.ndim == 1) , "State dim should be 1D np.array and size equal to num of degree of freedom"
        self.init_pos = pos
        self.reset()

    def compute_next_commands(self):
        assert (self.controller is not None),"Controller not found in the robot: " + self.name
        return self.controller.compute(self)

    def set_actuator_type(self, ctrl_type):
        self.actuator_type = ctrl_type
        if ctrl_type == Robot.SERVO:
            for jnt in self.skel.joints:
                jtype = jnt.type()
                if jtype != "WeldJoint":
                    # print jnt.type() 
                    jnt.set_actuator_type(2)
    
    def get_positions(self,):
        return self.skel.q
    
    def get_velocities(self,):
        return self.skel.dq
    
    def set_positions(self, q):
        self.skel.set_positions(q)
    
    def set_velocities(self, dq):
        self.skel.set_velocities(dq)
    
    def set_forces(self, tau):
        self.skel.set_forces(tau)

    def get_dofs(self,):
        return self.skel.dofs
    
    def num_dofs(self,):
        return self.skel.ndofs

    def get_joints(self,):
        return self.skel.joints
    
    def num_joints(self,):
        return self.skel.njoints
    
    def force_upper_limit(self):
        return self.skel.tau_upper
    
    def force_lower_limit(self):
        return self.skel.tau_lower

    
    def enforce_force_limits(self, commands):
        min =  np.minimum(self.force_upper_limit(),commands)
        return np.maximum(self.force_lower_limit(), min)
    
    # def servo_control(self, commands):
    #     diff = commands - self.get_velocities()
    #     self.integral += diff 
    #     force_commands = self.enforce_force_limits(diff * 10 + 2 * self.integral)
    #     self.set_forces(force_commands)

    def set_commands(self, commands):
        
        self.commands = commands

        if self.actuator_type == Robot.FORCE:
            self.set_forces(self.enforce_force_limits(commands))

        elif self.actuator_type == Robot.POSITION:
            self.set_positions(commands)
        
        elif self.actuator_type == Robot.VELOCITY:
            self.set_velocities(commands)

        elif self.actuator_type == Robot.FREE:
            pass
        
        elif self.actuator_type == Robot.SERVO:
            # Every world step resets the command values
            self.skel.set_commands(commands)


    def get_commands(self):

        if self.actuator_type == Robot.FORCE:
            return self.commands

        elif self.actuator_type == Robot.POSITION:
            return self.commands
        
        elif self.actuator_type == Robot.VELOCITY:
            return self.commands

        elif self.actuator_type == Robot.FREE:
            return self.commands
        
        elif self.actuator_type == Robot.SERVO:
            return np.array(self.skel.commands()) 
