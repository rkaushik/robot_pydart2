import OpenGL.GLUT.freeglut
from pydart2.gui.glut.window import *
from robot_pydart2.robot_world import *
import pydart2.utils.transformations as trans

class Simulator(GLUTWindow):
    def __init__(self, dt, title=None, default_camera=None, simulation_time = 40, display_fps=50):
        #dt dynamics simulation timestep
        #dt is in seconds
        self.skels = list()
        self.dt = dt
        self.sim = Robot_world(dt)
        self.sim.set_collision_detector(self.sim.BULLET_COLLISION_DETECTOR)
        self.title = title if title is not None else "GLUT Window"
        self.window_size = (1280, 720)
        self.scene = OpenGLScene(*self.window_size)

        self.mouseLastPos = None
        self.is_animating = False
        self.frame_index = 0
        self.capture_index = 0
        self.default_camera = default_camera
        if default_camera is not None:
            self.scene.set_camera(default_camera)

        self.robots = list()
        self.is_simulating = True
        
        # self.display_frame_rate = int(dt*1000.) #in ms
        self.simulation_time = simulation_time
        self.time_counter = 0.
        self.GLInitialized = False
        self.fps = display_fps
        self.reset()
    
    def reset(self):
        self.is_simulating = True
        self.is_animating = False
        self.frame_index = 0
        self.capture_index = 0
        self.time_counter = 0.
 
        for bot in self.robots:
            bot.reset()
        
    def num_robots(self):
        return len(self.robots)
    
    def get_robots(self):
        return self.robots

    def step(self):
        if self.time_counter > self.simulation_time:
           self.is_simulating = False
        else:
            for robot in self.robots:
                if robot.controller is not None and self.time_counter >= robot.control_timer:
                    robot.control_timer += robot.control_rate
                    commands = robot.compute_next_commands()
                    robot.set_commands(commands)
                else:
                    robot.set_commands(robot.commands) #set old command
            self.sim.step()
            self.time_counter += self.dt

    def idle(self, ):
        if self.sim is None:
            return

        if self.is_simulating:
            self.step()

        elif self.is_animating:
            self.frame_index = (self.frame_index + 1) % self.sim.num_frames()
            if hasattr(self.sim, "set_frame"):
                self.sim.set_frame(self.frame_index)
        
    def keyPressed(self, key, x, y):
        keycode = ord(key)
        key = key.decode('utf-8')

        if keycode == 27:
            GLUT.glutDestroyWindow(self.window)
            # sys.exit()
    
    def renderTimer(self, timer):
        #update physics
        self.idle()
        #Forward physics world time for one step
        self.time_counter += self.dt
        #check if time to display
        if self.time_counter >= self.next_display_time : 
            # glutPostRedisplay() essentially sets a flag 
            # so that on the next iteration of the mainloop, 
            # your registered display() function is called.
            GLUT.glutPostRedisplay()
            self.next_display_time += 1/float(self.fps)
        
        #Call the loop after a delay speficified in millisec
        GLUT.glutTimerFunc(int(self.dt*1000.), self.renderTimer, 1)
    
    def initGL(self, w, h):
        self.scene.init()

    def resizeGL(self, w, h):
        self.scene.resize(w, h)

    def drawGL(self, ):
        self.scene.render(self.sim)
        # GLUT.glutSolidSphere(0.3, 20, 20)  # Default object for debugging
        GLUT.glutSwapBuffers()
    
    def run(self, ):
   
        if not self.GLInitialized:
            GLUT.glutInit(())
            GLUT.glutInitDisplayMode(GLUT.GLUT_RGBA |
                                    GLUT.GLUT_DOUBLE |
                                    GLUT.GLUT_MULTISAMPLE |
                                    GLUT.GLUT_ALPHA |
                                    GLUT.GLUT_DEPTH)

            GLUT.glutInitWindowSize(*self.window_size)
            GLUT.glutInitWindowPosition(0, 0)
            self.window = GLUT.glutCreateWindow(self.title)

            # Init functions
            # glutFullScreen()
            GLUT.glutDisplayFunc(self.drawGL)
            # GLUT.glutIdleFunc(self.idle)
            GLUT.glutReshapeFunc(self.resizeGL)
            GLUT.glutKeyboardFunc(self.keyPressed)
            GLUT.glutMouseFunc(self.mouseFunc)
            GLUT.glutMotionFunc(self.motionFunc)
            if self.GLInitialized is not True:
                GLUT.glutTimerFunc(int(self.dt*1000.), self.renderTimer, 1)
            self.initGL(*self.window_size)
            self.GLInitialized = True
        
        # Run
        GLUT.glutShowWindow()
        while self.is_simulating: 
            GLUT.glutMainLoopEvent()  
        
        #Add hide window callback
        GLUT.glutHideWindow()
        #Add destroy window callback
        # GLUT.glutDestroyWindow(GLUT.glutGetWindow())

        #Call mainloop events so that window is hidden
        GLUT.glutMainLoopEvent()
        GLUT.glutMainLoopEvent()
        GLUT.glutMainLoopEvent()
        GLUT.glutMainLoopEvent()

    #Unactuated skeletons to the simulation
    def add_skeleton(self,skel_path, rpy_and_pos, fixed_to_world = True):
        skel = self.sim.add_skeleton(skel_path)
        pose = skel.positions()
        if not fixed_to_world:
            for i in range(6):
                pose[i] = rpy_and_pos[i]
            skel.set_positions(pose)
        
        else:
            Rot = trans.euler_matrix(rpy_and_pos[0], rpy_and_pos[1], rpy_and_pos[2], 'rxyz')
            Tr = trans.translation_matrix(rpy_and_pos[3:])
            T = trans.concatenate_matrices(Tr, Rot)
            skel.set_root_joint_to_weld()
            skel.root_bodynode().parent_joint.set_transform_from_parent_body_node(T);

        for j in skel.joints:
                j.set_position_limit_enforced()

        self.skels.append(skel)