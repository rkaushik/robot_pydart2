import sys
path = sys.path[0] + '/..'
sys.path.append(path)
# print sys.path


from robot_pydart2.simulator import Simulator
from robot_pydart2.robot import Robot
from robot_pydart2.controller import Controller

simu = Simulator(1/30., default_camera = 1, simulation_time = 1)
print("Simulator init OK")

ctlr = Controller()
print ("Controller created")

drawer_bot = Robot(skel_path = './urdf/drawer.urdf', name = "Drawer", controller = ctlr, simulator = simu)
drawer_bot.set_actuator_type(Robot.SERVO)

print("drawer_bot created OK")

simu.run()

simu.reset()

print ("Simulation reset")

simu.run()

simu.reset()

print ("Simulation reset")

simu.run()

print ("Simulation finished")