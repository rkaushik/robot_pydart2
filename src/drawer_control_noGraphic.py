import sys
path = sys.path[0] + '/..'
sys.path.append(path)
print sys.path


from robot_pydart2.simulator_noGraphic import Simulator
from robot_pydart2.robot import Robot
from robot_pydart2.controller import Controller
import numpy as np

simu = Simulator(1/30., default_camera = 1, simulation_time = 1)
print("Simulator init OK")

ctlr = Controller()
print ("Controller created")

drawer_bot = Robot(skel_path = './urdf/drawer.urdf', name = "Drawer",  simulator = simu)
drawer_bot.set_actuator_type(Robot.SERVO)

print("drawer_bot created OK")

for i in range(100):
    simu.step(np.array([[0.3]]))
    # print ("pos:", simu.get_robots()[0].get_positions())
    # print ("vel:", simu.get_robots()[0].get_velocities())
    print (simu.get_robots()[0].get_positions())

simu.reset()
print ("Simulation reset")

for i in range(100):
    simu.step(np.array([[0.3]]))
    # print ("pos:", simu.get_robots()[0].get_positions())
    # print ("vel:", simu.get_robots()[0].get_velocities())
    print (simu.get_robots()[0].get_positions())


simu.reset()
print ("Simulation reset")

for i in range(100):
    simu.step(np.array([[0.3]]))
    # print ("pos:", simu.get_robots()[0].get_positions())
    # print ("vel:", simu.get_robots()[0].get_velocities())
    print (simu.get_robots()[0].get_positions())


print ("Simulation finished")

simu.run()