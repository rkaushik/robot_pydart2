import sys
path = sys.path[0] + '/..'
sys.path.append(path)
# print sys.path

import numpy as np
# from robot_pydart2.simulator_noGraphic import Simulator
from robot_pydart2.simulator import Simulator
from robot_pydart2.robot import Robot
from robot_pydart2.controller import Controller
import pydart2.utils.transformations as trans
import os 

class Controller(object):
    def __init__(self):
        self.integral = np.array([0.])
    
    def reset(self):
        self.integral = np.array([0.])
    def compute(self, robot):
        return np.array([-0.1, 0.3])

ctlr = Controller()
this_dir = os.path.dirname(os.path.realpath(__file__))
_simu = Simulator(1/30., default_camera = 1, simulation_time = 10)
_arm_bot = Robot(skel_path = this_dir + '/../urdf/omnigrasper_squarePeg_2d.urdf', controller = ctlr, name = "arm_2dof", simulator = _simu)
_arm_bot.set_actuator_type(Robot.SERVO)

# for i in range(20):
#     _simu.step(np.array([[2.0,2.0]]))
#     print _simu.get_robots()[0].get_positions()
#     print _simu.get_robots()[0].commands
#     print _simu.get_robots()[0].skel.dq
#     print _simu.get_robots()[0].get_velocities()

_simu.run()