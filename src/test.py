class Skel:
    def __init__(self, name):
        self.name = name
    def state(self, num):
        self.state = num

class World:
    
    def __init__(self, world_name):
        self.name = world_name
        self.skels = []
        self.robots = []

    def add_skel(self, name):
        skel = Skel(name)
        self.skels.append(skel)
        return skel

class Robot:
    def __init__(self, name, world):
        self.name = name
        world.add_skel(name)
        self.skel_id = len(world.skels) - 1
        self.world = world
        self.skel = world.skels[self.skel_id]
        world.robots.append(self)

world  = World("World_1")
robot1 = Robot("R1", world)
robot2 = Robot("R2", world)

robot2.skel.name = "R2_newname"
robot2.name = "R2_newname"

for s in world.skels:
    print s.name

for r in world.robots:
    print r.name



